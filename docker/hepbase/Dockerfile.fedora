FROM fedora:32
LABEL maintainer="rivet-developers@cern.ch"

ARG HEPMC_VERSION=2
ARG CXX_CMD=g++
ARG CC_CMD=gcc
ARG FC_CMD=gfortran
ARG RIVET_VERSION=3.1.1

ENV CXX=${CXX_CMD}
ENV CC=${CC_CMD}
ENV FC=${FC_CMD}

RUN true \
    && if test "$CXX_CMD" = "g++"; then CXX_PKG=gcc-c++; else CXX_PKG=clang; fi \
    && if test "$CC_CMD" = "gcc"; then CC_PKG=gcc; else CC_PKG=clang; fi \
    && if test "$FC_CMD" = "gfortran"; then FC_PKG=gcc-gfortran; else FC_PKG=flang; fi \
    && dnf install -y ${CXX_PKG} ${CC_PKG} ${FC_PKG} \
    && if test "$CXX_CMD" = "clang++"; then alternatives --install /usr/bin/g++ g++ /usr/bin/clang++ 2; fi \
    && if test "$CXX_CMD" = "clang++"; then alternatives --install /usr/bin/c++ c++ /usr/bin/clang++ 2; fi \
    && if test "$CC_CMD" = "clang"; then alternatives --install /usr/bin/gcc gcc /usr/bin/clang 2; fi \
    && if test "$CC_CMD" = "clang"; then alternatives --install /usr/bin/cc cc /usr/bin/clang 2; fi \
    && if test "$FC_CMD" = "flang"; then alternatives --install /usr/bin/gfortran gfortran /usr/bin/flang 2; fi \
    && dnf install -y \
      make redhat-rpm-config cmake \
      wget tar less bzip2 findutils which nano file \
      zlib-devel gsl-devel \
    && dnf install -y python3 python3-devel python3-pip \
    && alternatives --install /usr/bin/python python /usr/bin/python3 2 \
    && pip install matplotlib requests Cython \
    && dnf clean all

# RUN dnf install -y \
#       texlive-latex-bin texlive-texconfig-bin texlive-pst-tools texlive-pst-arrow \
#       texlive-relsize texlive-cm texlive-hyphen-base texlive-collection-fontsrecommended \
#       ghostscript GraphicsMagick texlive-dvips \
#     && dnf clean all

RUN true \
    && if test "$HEPMC_VERSION" = "3"; then HEPMC_FULL_VERSION=3.2.1; else HEPMC_FULL_VERSION=2.06.10; fi \
    && mkdir /code && cd /code \
    && wget https://gitlab.com/hepcedar/rivetbootstrap/raw/${RIVET_VERSION}/rivet-bootstrap \
    && chmod +x rivet-bootstrap \
    && INSTALL_PREFIX=/usr/local INSTALL_RIVET=0 INSTALL_CYTHON=0 HEPMC_VERSION=${HEPMC_FULL_VERSION} MAKE="make -j6" ./rivet-bootstrap \
    && rm -rf /code

#ENV LD_LIBRARY_PATH /usr/local/lib
#ENV PYTHONPATH /usr/local/lib64/python2.7/site-packages

WORKDIR /work
