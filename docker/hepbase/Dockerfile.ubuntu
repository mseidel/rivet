FROM ubuntu:20.04
LABEL maintainer="rivet-developers@cern.ch"

ARG HEPMC_VERSION=2
ARG CXX_CMD=g++
ARG CC_CMD=gcc
ARG FC_CMD=gfortran
ARG RIVET_VERSION=3.1.1

ENV CXX=${CXX_CMD}
ENV CC=${CC_CMD}
ENV FC=${FC_CMD}

RUN export DEBIAN_FRONTEND=noninteractive \
    && if test "$CXX_CMD" = "g++"; then CXX_PKG=g++; else CXX_PKG=clang; fi \
    && if test "$CC_CMD" = "gcc"; then CC_PKG=gcc; else CC_PKG=clang; fi \
    && if test "$FC_CMD" = "gfortran"; then FC_PKG=gfortran; else FC_PKG=flang; fi \
    && apt-get update -y \
    && apt-get install -y ${CXX_PKG} ${CC_PKG} ${FC_PKG} \
    && if test "$CXX_CMD" = "clang++"; then update-alternatives --install /usr/bin/g++ g++ /usr/bin/clang++ 2; fi \
    && if test "$CXX_CMD" = "clang++"; then update-alternatives --install /usr/bin/c++ c++ /usr/bin/clang++ 2; fi \
    && if test "$CC_CMD" = "clang"; then update-alternatives --install /usr/bin/gcc gcc /usr/bin/clang 2; fi \
    && if test "$CC_CMD" = "clang"; then update-alternatives --install /usr/bin/cc cc /usr/bin/clang 2; fi \
    && if test "$FC_CMD" = "flang"; then update-alternatives --install /usr/bin/gfortran gfortran /usr/bin/flang 2; fi \
    && apt-get install -y \
      make automake autoconf libtool cmake \
      wget tar less bzip2 findutils nano file \
      zlib1g-dev libgsl-dev \
    && apt-get install -y python3 python3-dev python3-pip \
    && update-alternatives --install /usr/bin/python python /usr/bin/python3 2 \
    && pip3 install matplotlib requests Cython \
    && apt-get clean -y

RUN mkdir /code && cd /code \
    && if test "$HEPMC_VERSION" = "3"; then HEPMC_FULL_VERSION=3.2.1; else HEPMC_FULL_VERSION=2.06.10; fi \
    && wget https://gitlab.com/hepcedar/rivetbootstrap/raw/${RIVET_VERSION}/rivet-bootstrap \
    && chmod +x rivet-bootstrap \
    && INSTALL_PREFIX=/usr/local INSTALL_RIVET=0 INSTALL_CYTHON=0 HEPMC_VERSION=${HEPMC_FULL_VERSION} MAKE="make -j6" ./rivet-bootstrap \
    && rm -rf /code

# ENV LD_LIBRARY_PATH /usr/local/lib
# ENV PYTHONPATH /usr/local/lib64/python2.7/site-packages

WORKDIR /work
