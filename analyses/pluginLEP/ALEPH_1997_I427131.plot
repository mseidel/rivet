BEGIN PLOT /ALEPH_1997_I427131/d02-x01-y02
Title=$\pi^0$ scaled momentum spectrum
XLabel=$x_p$
YLabel=$\text{d}\sigma/\text{d}x_p$
END PLOT

BEGIN PLOT /ALEPH_1997_I427131/d03-x01-y01
Title=$\pi^0$ $p_T^{\text{out}}$ w.r.t sphericity axis
XLabel=$p_T^{\text{out}}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_T^{\text{out}}$ [$\text{GeV}^{-1}$]
END PLOT

BEGIN PLOT /ALEPH_1997_I427131/d04-x01-y01
Title=$\pi^0$ $p_T^{\text{in}}$ w.r.t sphericity axis
XLabel=$p_T^{\text{in}}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_T^{\text{in}}$ [$\text{GeV}^{-1}$]
END PLOT
