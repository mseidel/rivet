BEGIN PLOT /CLEOII_1994_I381696/d05-x01-y01
Title=Spectrum for $\Lambda_c(2595)^+$ production
XLabel=$x_p$
YLabel=$1/N\text{d}N/\text{d}x_p$
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1994_I381696/d06-x01-y01
Title=Spectrum for $\Lambda_c(2625)^+$ production
XLabel=$x_p$
YLabel=$1/N\text{d}N/\text{d}x_p$
LogY=0
END PLOT
