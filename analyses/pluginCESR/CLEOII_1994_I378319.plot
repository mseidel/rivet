BEGIN PLOT /CLEOII_1994_I378319/d04-x01-y01
Title=Scaled momentum spectrum for $D_2(2460)^+$ production
XLabel=$x_p$
YLabel=$1/N\text{d}N/\text{d}x_p$
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1994_I378319/d04-x01-y02
Title=Scaled momentum spectrum for $D_1(2420)^+$ production
XLabel=$x_p$
YLabel=$1/N\text{d}N/\text{d}x_p$
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1994_I378319/d02-x01-y01
Title=Helicity Angle for $D_2(2460)^+\to D^{*0}(\to D^0\pi^0)\pi^+$
XLabel=$\cos\theta$
YLabel=$1/N\text{d}N/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1994_I378319/d03-x01-y01
Title=Helicity Angle for $D_1(2420)^+\to D^{*0}(\to D^0\pi^0)\pi^+$
XLabel=$\cos\theta$
YLabel=$1/N\text{d}N/\text{d}\cos\theta$
LogY=0
END PLOT