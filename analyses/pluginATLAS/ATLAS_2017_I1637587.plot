# BEGIN PLOT /ATLAS_2017_I1637587/d.
LogY=0
LegendAlign=r
XTwosidedTicks=1
YTwosidedTicks=1
LeftMargin=2.0
YLabelSep=8.0
# END PLOT

# BEGIN PLOT /ATLAS_2017_I1637587/d01
Title=Soft drop $\beta = 0$, $z_\text{cut}=0.1$, $p_\text{T}^\text{lead} > 600$ GeV
XLabel=$\text{log}_{10}[(m^\text{soft\,drop} / p_\text{T}^\text{ungroomed})^2]$
YLabel=$\dfrac{1}{\sigma_\text{resum}} \; \dfrac{\text{d}\sigma}{\text{d} \text{log}_{10}[(m^\text{soft\,drop} / p_\text{T}^\text{ungroomed})^2]}$
LegendAlign=l
LegendXPos=0.4
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ATLAS_2017_I1637587/d02
Title=Soft drop $\beta = 1$, $z_\text{cut}=0.1$, $p_\text{T}^\text{lead} > 600$ GeV
XLabel=$\text{log}_{10}[(m^\text{soft\,drop} / p_\text{T}^\text{ungroomed})^2]$
YLabel=$\dfrac{1}{\sigma_\text{resum}} \; \dfrac{\text{d}\sigma}{\text{d} \text{log}_{10}[(m^\text{soft\,drop} / p_\text{T}^\text{ungroomed})^2]}$
LegendAlign=l
LegendXPos=0.4
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ATLAS_2017_I1637587/d03
Title=Soft drop $\beta = 2$, $z_\text{cut}=0.1$, $p_\text{T}^\text{lead} > 600$ GeV
XLabel=$\text{log}_{10}[(m^\text{soft\,drop} / p_\text{T}^\text{ungroomed})^2]$
YLabel=$\dfrac{1}{\sigma_\text{resum}} \; \dfrac{\text{d}\sigma}{\text{d} \text{log}_{10}[(m^\text{soft\,drop} / p_\text{T}^\text{ungroomed})^2]}$
LegendAlign=l
LegendXPos=0.4
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /ATLAS_2017_I1637587/d04
Title=Soft drop $\beta = 0$, $z_\text{cut}=0.1$
XLabel=$\text{log}_{10}[(m^\text{soft\,drop} / p_\text{T}^\text{ungroomed})^2]$
YLabel=$\dfrac{1}{\sigma_\text{resum}} \; \dfrac{\text{d}\sigma}{\text{d} \text{log}_{10}[(m^\text{soft\,drop} / p_\text{T}^\text{ungroomed})^2]}$
LogY=1
YMax=1.0
# END PLOT

# BEGIN PLOT /ATLAS_2017_I1637587/d05
Title=Soft drop $\beta = 1$, $z_\text{cut}=0.1$
XLabel=$\text{log}_{10}[(m^\text{soft\,drop} / p_\text{T}^\text{ungroomed})^2]$
YLabel=$\dfrac{1}{\sigma_\text{resum}} \; \dfrac{\text{d}\sigma}{\text{d} \text{log}_{10}[(m^\text{soft\,drop} / p_\text{T}^\text{ungroomed})^2]}$
LogY=1
YMax=3.0
# END PLOT

# BEGIN PLOT /ATLAS_2017_I1637587/d06
Title=Soft drop $\beta = 2$, $z_\text{cut}=0.1$
XLabel=$\text{log}_{10}[(m^\text{soft\,drop} / p_\text{T}^\text{ungroomed})^2]$
YLabel=$\dfrac{1}{\sigma_\text{resum}} \; \dfrac{\text{d}\sigma}{\text{d} \text{log}_{10}[(m^\text{soft\,drop} / p_\text{T}^\text{ungroomed})^2]}$
LogY=1
YMax=5.0
# END PLOT

